package rahman.aulya.appx06

import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener  {

    lateinit var fragProdi : fragmentprodi
    lateinit var fragMhs : fragmentmahasiswa
    lateinit var ft : FragmentTransaction
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
            bottomNavigationView.setOnNavigationItemSelectedListener(this)
            fragProdi = fragmentprodi()
            fragMhs = fragmentmahasiswa()
        }
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemprodi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragProdi).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,255,225))
                framelayout.visibility = View.VISIBLE
            }
            R.id.itemmhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.framelayout,fragMhs).commit()
                framelayout.setBackgroundColor(Color.argb(245,255,225,255))
                framelayout.visibility = View.VISIBLE
            }
            R.id.itemabout -> framelayout.visibility = View.GONE
        }
        return true
    }
}
